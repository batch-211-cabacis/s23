console.log("Hello, B211!");

// Object
/*
	An object is a data type that is used to represent real world objects
	It is a collection of related data and/or functionalities
	Information stored in objects are represented in a "key:value" pair

	"key" - "property" of an object
	"value" - actual data to be stored

	Different data type may be stored in an object's property creating complex data structures

*/

// there are two ways in creating objects in JS
	// 1. Object Literal notation 
			// (let object = {} **curly braces**
	// 2. Object constructor notation 
			// Object instantation (let object = new object())


// Object Literal Notation
	// creating objects using initializer/literal notation
	// camelCase
	// SYNTAX 
		/* let objectName = {
			keyA: valueA
			keyB: valueB
		}
		*/

		let cellphone = {
			name: "Nokia 3210",
			manufactureDate: 1999
		};
		console.log('Result from creating objects using literal notation');
		console.log(cellphone);

// //  Mini Activity 1

// 		let cellphone2 = {
// 			name: "Iphone 14"
// 			manufactureDate: 2022;
// 		};
// 		console.log('Result from creating objects using literal notation')
// 		console.log(cellphone2);

			// let ninja = {
			// 	name: "Naruto Uzumaki",
			// 	village: "Konoha",
			// 	children: ["Boruto", "Himawari"]
			// };
			// console.log(ninja);

		// Object Constructor Notation 
				// Creating a reusable "function" to create several objects

				/*
				SYNTAX

						function objectName(keyA,keyB){
							this.keyA = keyA;
							this.keyB = keyB;
						}
				*/

				// "this" keyword refers to the properties within the object
						// 

				function Laptop(name,manufactureDate){
							this.name = name;
							this.manufactureDate = manufactureDate;
				}

				let laptop = new Laptop("Lenovo",2008);
				console.log("Result from creating objects using object constructor")
				console.log(laptop);

				// the new operator creates an instance of an object (new object)

				let myLaptop = new Laptop("Macbook Air",2020);
				console.log("Result from creating objects using object constructor");
				console.log(myLaptop);


				// Mini Activity 2

				let laptop1 = new Laptop("Asus",2021);
				console.log(laptop1);

				let laptop2 = new Laptop("HP",2020);
				console.log(laptop2);

				let laptop3 = new Laptop("Dell",2019);
				console.log(laptop3);

				let oldLaptop = Laptop("Portal R2E CCMC", 1980);
				console.log("Result from creating objects using object constructor")
				console.log(oldLaptop); //undefined

				// Create empty objects

				let computer = {};
				let myComputer = new Object();

				console.log(computer);
				console.log(myComputer);



				// Accessing Object Properties
				// Using dot notation 
					console.log("Result from dot quotation: " + myLaptop.name); // highly recommended to use

				// Using square bracket notation
					console.log("Result from square bracket quotation: " + myLaptop["name"]);

				// Access array of objects
				// Accessing object properties using the square bracket notation and array indexes can cause confusion

				let arrayObject = [laptop,myLaptop];
				// may be consued for accessing array indexes
				console.log(arrayObject[0]["name"]);
				// this tells us that array[0] is an object by using the dot notation
				console.log(arrayObject[0].name);


				// Initializing/Adding/Deleting/Reassigning Object Properties
				// Create an object using object literals
				let car = {};
				console.log("Current value of car object: ");
				console.log(car); //{}

				// Initializing/Adding object properties
				car.name = "Honda Civic";
				console.log("Result from adding properties using dot notation: ");
				console.log(car); 

				// Initializing/adding object properties using bracket notation (not recommended)

				console.log("Result from Adding a property using a squar bracket notation:");
				car["manufacture date"] = 2019;
				console.log(car);

				// Deleting object proprties
				delete car["manufacture date"];
				console.log("Result from deleting properties: ");
				console.log(car);

				car["manufactureDate"] = 2019;
				console.log(car);

				// Reassigning object property values

				car.name = "Toyota Vios";
				console.log("Result from reassigning property values: ");
				console.log(car);

				delete car.manufactureDate
				console.log(car);

				// Object Methods
					// a method is a function which is a property of an object

					let person = {
						name: "John",
						talk: function(){
							console.log("Hello my name is " + this.name);
						}
					}
					console.log(person);
					console.log("Result from object methods: ");
					person.talk();

					person.walk = function(steps){
						console.log(this.name + " walk "  + steps + " steps forward");
					}
					console.log(person);
					person.walk(50);

					// Methods are useful for creating reusable functions that perform tasks related to objects

					let friend = {
						firstName: "Joe",
						lastName: "Smith",
						address: {
							city: "Austin",
							country: "Texas"
						},
						emails: ["joe@mail.com", "joesmith@mail.xyz"],
							introduce: function(){
							console.log("Hello my name is " + this.firstName + " " + this.lastName + " " + "I live in " + this.address.city + ", " + this.address.country)
						}
					}
					friend.introduce();

					// Real world application of objects
					/*
						Scenario:
						1. we would like to create a game that would have several Pokemon interact with eah other
						2. Every pokemon would have the same set of stats, properties, and function

						Stats:
						name:
						level:
						health: level * 2
						attack: level
					*/

					// Create an object constructor to lessen the process in creating the pokemon


					function Pokemon(name,level){
						// properties
						this.name = name;
						this.level = level;
						this.health = level * 2;
						this.attack = level;
						this.tackle = function(target){
								console.log(this.name + " tackled " + target.name)

								// Mini activity 3

								target.health -= this.attack
								// target.health = target.health - this.attack	
								console.log(target.name + " health is now reduced to " + target.health);

								if(target.health <= 0){
									target.faint()

								}

						}
					this.faint = function(){
						console.log(this.name + " fainted.")

					}


					}

					let pikachu = new Pokemon ("Pikachu", 88);
					console.log(pikachu);

					let rattata = new Pokemon ("Rattata", 10);
					console.log(rattata);
